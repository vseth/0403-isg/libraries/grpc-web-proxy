# grpc web proxy

[GRPC web proxy]("https://github.com/improbable-eng/grpc-web") containerized for local development.

Simply set the environment variable `BACKENDADDR` to the backend address for the proxy and you are set.

Note:
Since this is intended for local development only, SSL is disabled.
