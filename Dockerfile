FROM golang:1.17-alpine3.16
RUN go get -u github.com/improbable-eng/grpc-web/go/grpcwebproxy@v0.14.1

# max receive message at 1GB
CMD [ "sh", "-c", \
    "/go/bin/grpcwebproxy --backend_addr=$BACKENDADDR --run_tls_server=false --allow_all_origins --backend_max_call_recv_msg_size=1073741824"]
